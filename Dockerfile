FROM node:alpine

COPY package.json yarn.lock /tmp/
RUN cd /tmp && yarn install --frozen-lockfile
RUN mkdir -p /app && mv /tmp/node_modules /app/

# copy application files
COPY . /app
WORKDIR /app

# CMD
CMD yarn run serve
